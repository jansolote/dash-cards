/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../dash-cards.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<dash-cards></dash-cards>");
    assert.strictEqual(_element.hello, "Hello World!");
  });
});
