import { html, css, LitElement } from "lit-element";

class DashCards extends LitElement {
  static get properties() {
    return {
      cardName:{type:String},
      cardImage:{type:String},
      textColor:{type:String}

    };
  }

  constructor() {
    super();
    this.cardName=""
    this.cardImage="";
    this.textColor="";
  }

  static get styles() {
    return css`
    @import url('https://fonts.googleapis.com/css?family=Roboto+Condensed');

      .card-container{
        border-radius:10px;
        display:flex;
        flex-flow:column wrap;
        justify-content:center;
        align-items:center;
        cursor:pointer;
        @apply --dash-cards-mixin;
      }
      .cardImage{
        width:80px;
        background-color:rgba(249, 250, 252,0.5);
        @apply --dash-cards-image;
      }
      .cardTitle{
        font-family: 'Roboto Condensed', sans-serif;

      }
    `;
  }

  render() {
    return html`
      <div class="card-container" @click="${this.cardClicked}">
            <img class="cardImage" src="${this.cardImage}" alt="">
            <p class="cardTitle" style="color:${this.textColor}">${this.cardName}</p>
      </div>
    `;
  }

  cardClicked(event){
    this.dispatchEvent(new CustomEvent("card-clicked",{
      bubbles:false,
      composed:false,
      detail:this.cardName
    }))
  }
}

window.customElements.define("dash-cards", DashCards);
